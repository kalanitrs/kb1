<?php

namespace app\models;
use yii\db\ActiveRecord;

class Product extends ActiveRecord
{
    /*public $id;
    public $name;
    public $price;

    private $products = [
        '100' => [
            'id' => '100',
            'name' => 'Table',
            'price' => '22',
        ],
        '101' => [
            'id' => '101',
            'name' => 'Chair',
            'price' => '12',
        ],
        '102' => [
            'id' => '102',
            'name' => 'Ram',
            'price' => '7',
        ]
    ];

    public function getAll(){
        return $this-> products;
    }
   */
  public static function tableName() {
      return 'product';
  }
}